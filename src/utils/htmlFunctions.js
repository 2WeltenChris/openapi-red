const setError = (node, text, source) => {
  console.log('[openAPI-red]', text)
  node.internalErrors.text = text
  node.internalErrors.source = source
}

const clearError = (node, source) => {
  if (!source || node.internalErrors.source === source) {
    node.internalErrors.text = ''
    node.internalErrors.source = ''
  }
}

const getMultipleSchemesType = (paramOrSchemaData, inArray) => {
  let schema
  if (inArray) {
    schema = paramOrSchemaData?.schema || paramOrSchemaData?.items?.schema || paramOrSchemaData?.items || paramOrSchemaData
  } else {
    schema = paramOrSchemaData?.schema || paramOrSchemaData
  }
  // only handling the first one found!
  if (schema?.oneOf?.length) {
    return 'oneOf'
  // anyOf shows only one scheme like oneOf, if user needs more than this scheme input
  // he must use a normal json editor
  } else if (schema?.anyOf?.length) {
    return 'anyOf'
  // handle allOf only if it has one entry (which is equal to oneOf)
  // else the schemes must be merged (like anyOf)
  } else if (schema?.allOf?.length === 1) {
    return 'allOf'
  }
}

const getMultiSelectionSelectedSchema = (parameter, paramMetaData) => {
  // is linked to a schema?
  if (!parameter.selectedSchema?.name) {
    return {}
  }
  // note: selecteable schema can also be a simple value or object parameter
  const multiSchemesList = paramMetaData.schema?.[parameter.selectedSchema?.type] || paramMetaData?.[parameter.selectedSchema?.type]
  const newSchema = multiSchemesList?.find(s => s?.$$ref?.endsWith('/' + parameter.selectedSchema?.name))
  if (newSchema) {
    return newSchema
  } else {
    console.warn('[openAPI-red] Warning: selected schema not found.')
    return {}
  }
}

const getTypedInputType = (paramMetaData = {}, getEnum = true) => {
  // swagger has schema nested, openApi has schema directly
  const multiSchemesType = (getMultipleSchemesType(paramMetaData))
  // TODO: check for other scheme types after found one? currently only allows one type, mixed can become very complex and there won't be propably a usecase.
  // paramMetaData[mulitSchemesType] is for 'select' arrays with schemes.
  const schema = paramMetaData.schema?.[multiSchemesType]?.[0] || paramMetaData[multiSchemesType]?.[0] || paramMetaData.schema
  let type = schema?.type || paramMetaData.type
  if (type?.oneOf) {
    type = type.oneOf[0]
  }
  const nullableBool = (type === 'boolean' && (schema?.nullable || paramMetaData.nullable))
  const hasEnum = (type !== 'boolean' && (paramMetaData.enum?.length || paramMetaData.items?.enum?.length || schema?.enum?.length))
  if (getEnum && (hasEnum || nullableBool)) {
    return 'select'
  }
  if (type === 'boolean') {
    return 'bool' // nullable bool -> select
  } else if (type === 'integer') {
    return 'num'
  } else if (paramMetaData.name === 'Json Request Body' || paramMetaData.name === 'body' || type === 'body' || type === 'object') {
    // return "editor" will lead in parameter row to the check for multiple editors and the editor name and select the first one
    return (schema?.properties || paramMetaData.properties) ? 'editor' : 'json'
  } else if (type === 'array' && (schema?.items?.type || paramMetaData.items?.type || getMultipleSchemesType(paramMetaData.items || {}))) {
    // array should be defined what type is needed or can have nested multiple schemes inside
    return 'array'
  } else {
    return 'str'
  }
}

const createOutputs = (node, $specStore) => {
  node.outputs = 0
  node.responseOutputLabels = []
  const succesfulResponse = { code: 'successful', text: 'all successful responses (2xx)' }
  const allResponse = { code: 'all', text: 'all responses' }

  if (node.outputStyle === 'classic') {
    node.outputs++
    if (node.errorHandling === 'default') {
      node.responseOutputLabels.push(succesfulResponse)
    } else {
      node.responseOutputLabels.push(allResponse)
    }
  } else {
    const apiOperationData = $specStore[node.configUrlNode].paths?.[node.operationData.path]?.[node.operationData.method] || {}
    let hasSuccessfulOutput = false
    const responseCodes = Object.keys(apiOperationData.responses || {})

    if (node.outputStyle === 'each response') {
      responseCodes.sort().forEach(responseCode => {
        node.outputs++
        node.responseOutputLabels.push({ code: responseCode, text: apiOperationData.responses[responseCode].description })
        if (responseCode >= 200 && responseCode <= 299) {
          hasSuccessfulOutput = true
        }
      })
    } else if (node.outputStyle === 'compact') {
      const outputs = new Set()
      Object.keys(apiOperationData.responses || {}).forEach(responseCode => outputs.add(responseCode.toString().substring(0, 1) + 'xx'))
      hasSuccessfulOutput = outputs.has('2xx')
      node.responseOutputLabels = [...outputs].map(code => {
        node.outputs++
        return { code, text: 'Handles all response codes with the schema "' + code + '".' }
      })
    }

    // if no successful response is defined, create an all successful response output as first output
    if (!hasSuccessfulOutput) {
      node.outputs++
      node.responseOutputLabels.unshift(succesfulResponse)
    }
    // if no responses are defined, send all responses to one output, check if error handling has other output
    if (!node.outputs) {
      node.outputs++
      const label = node.errorHandling === 'default' ? 'all' : 'successful'
      if (label === 'all') {
        node.responseOutputLabels = [allResponse]
      } else if (label === 'successful') {
        node.responseOutputLabels = [succesfulResponse]
      }
    }
  }
  if (node.errorHandling === 'other output') {
    node.outputs++
    node.responseOutputLabels.push({ code: 'undefinedResponses', text: 'all undefined responses' })
  }
}

const getAllowedTypes = (metaOrSchema, paramName, additionalEachTypes) => {
  const type = getTypedInputType(metaOrSchema)
  const eachInputTypes = additionalEachTypes.map(asValue => { return { value: '_each_' + asValue, label: asValue } })
  let result = []
  switch (type) {
    case 'bool':
    case 'num':
    case 'json':
      result = [type, ...eachInputTypes, 'jsonata', 'msg', 'flow', 'global']
      break
    case 'editor': {
      // currently only handles the first type (oneOf, anyOf) found if multiple editors exists (while oneOf seems to be the most important)
      const reference = metaOrSchema?.$$ref || paramName || 'Unknown'
      const label = metaOrSchema?.['x-label']
        ? metaOrSchema?.['x-label']
        : reference.split('/').pop() + ' - Editor'
      const value = reference ? 'editor:' + reference.split('/').pop() : 'editor'
      result = [{ value, label, hasValue: false }, ...eachInputTypes, 'json', 'jsonata', 'msg', 'flow', 'global']
      break
    }
    case 'select': {
      // typedInput options must be string -> this means number/bool/null/etc. may loose their data type. (trying to JSON.stringify and JSON.parse it back)
      // if that fails the server must handle the wrong data type or the user has to enter the value directly or via msg.
      const defaultType = getTypedInputType(metaOrSchema, false)
      if (defaultType === 'bool') {
        // nullable bool
        result = [{ value: 'select', label: 'boolean or null', options: ['true', 'false', 'null'], icon: 'red/images/typedInput/bool.svg' }, ...eachInputTypes, 'jsonata', 'msg', 'flow', 'global']
      } else {
        const options = (metaOrSchema?.items?.enum || metaOrSchema?.schema?.enum || metaOrSchema?.enum || []).map(v => {
          const option = {}
          if (typeof v === 'undefined') {
            option.value = 'undefined'
          } else if (typeof v === 'string') {
            option.value = v // else value and label would be in quotes ("value" instead of value)
          } else {
            option.value = JSON.stringify(v)
          }
          option.label = option.label || option.value
          return option
        })
        result = [{ value: 'select', label: 'Select', options, icon: 'fa-list-ul' }, ...eachInputTypes, defaultType, 'json', 'jsonata', 'msg', 'flow', 'global']
      }
      break
    }
    case 'array':
      result = [{ value: 'each', label: 'Each', hasValue: false, icon: 'fa-tasks' }, { value: 'array', label: 'Array', hasValue: false, icon: 'fa-list-ol' }, ...eachInputTypes, 'json', 'msg', 'flow', 'global']
      break
    default:
      result = [...eachInputTypes, 'str', 'json', 'jsonata', 'msg', 'flow', 'global']
      break
  }
  return result.filter(type => type)
}

// if parameter has own parameters show them if parent type is editor and set default value to msg.payload[parentParam][param]
// can be parameter or subParameter
const createParameter = (name, oldParamValues = {}, paramMetaData = {}, requiredByParent = false, msgPathString = 'payload', newArrayValue = false) => {
  if (!isNaN(name) || name.includes(' ') || name.includes('@')) {
    msgPathString += '["' + name + '"]'
  } else {
    msgPathString += '.' + name
  }
  // init type (old value) or (editor (obj) || msg)
  let type = oldParamValues.type
  const defaultType = getTypedInputType(paramMetaData)
  if (!type) {
    if (defaultType === 'editor') {
      type = defaultType // editor type will be changed later to editor:SchemaName
    } else {
      type = 'msg'
    }
  }

  let required = requiredByParent
  let schema = paramMetaData?.schema || paramMetaData || {}
  let selectedSchema
  const multiSchemesType = getMultipleSchemesType(paramMetaData, newArrayValue)
  if (multiSchemesType) {
    selectedSchema = oldParamValues.selectedSchema || {}
    const selectedType = selectedSchema.type || multiSchemesType
    let selectedSchemaIndex = schema[multiSchemesType]?.findIndex(s => s?.$$ref?.endsWith(selectedSchema.name))
    selectedSchemaIndex = selectedSchemaIndex >= 0 ? selectedSchemaIndex : 0 // undefined >= 0 => false
    // set schema to nested schema if existing
    schema = schema[selectedType]?.[selectedSchemaIndex] || {}
    selectedSchema.type = selectedType
    selectedSchema.name = schema.title || schema.$$ref?.split('/')?.pop() || ''
  }

  if (!required) {
    if (typeof schema.required === 'boolean') {
      required = schema.required
    } else if (typeof paramMetaData.required === 'boolean') {
      required = paramMetaData.required
    }
  }

  const result = {
    name,
    isActive: required || requiredByParent || !!oldParamValues.isActive, // required in schema is directly for this parameter
    activeFilter: oldParamValues.activeFilter || '',
    required,
    type,
    collapsed: (typeof oldParamValues.collapsed !== 'undefined') ? oldParamValues.collapsed : true,
    value: (typeof oldParamValues.value !== 'undefined') ? oldParamValues.value : msgPathString,
    parameters: {},
    mark: oldParamValues.mark || { name: false, type: false, value: false, activeFilter: false },
    parent: oldParamValues.parent,
    msgPathString
  }
  // additional special data
  if (selectedSchema) {
    result.selectedSchema = selectedSchema
  }
  if (paramMetaData.in) {
    result.in = paramMetaData.in
  }
  // each is a "single" array component for easier handling
  if (result.type === 'array' || result.type === 'each') {
    result.value = result.value.map((v, i) => createParameter(i, v, paramMetaData.items, true, msgPathString))
  }
  if (result.type === 'each') {
    result.eachType = oldParamValues.eachType || 'msg'
    result.each = oldParamValues.each || ''
    result.as = oldParamValues.as || ''
  }
  // if parameter has properties...
  const nestedProperties = schema.properties || paramMetaData.properties || {}
  Object.keys(nestedProperties).forEach(nestedName => {
    const oldNestedParamVal = oldParamValues.parameters?.[nestedName]
    const nestedParamMetaData = nestedProperties[nestedName]
    const childParamsRequired = Array.isArray(schema?.required)
      ? schema.required
      : Array.isArray(paramMetaData.required)
        ? paramMetaData.required
        : []
    const reqByParent = childParamsRequired.includes(nestedName)
    result.parameters[nestedName] = createParameter(nestedName, oldNestedParamVal, nestedParamMetaData, reqByParent, msgPathString)
  })

  return result
}

const orderRequired = (a, b) => {
  let comparison = 0
  if (b.required) {
    comparison = 1
  } else if (a.required) {
    comparison = -1
  }
  return comparison
}

const createParameters = (node, apiOperationData = {}) => {
  if (!Object.keys(apiOperationData).length) {
    // changed api, but triggered to early. if parameters have same name, we want to keep the value
    return
  }
  try {
    const oldParameters = { ...node.parameters }
    node.parameters = {}
    if (node.internalErrors.source === 'createParameters') {
      clearError(node)
    }
    // openApi 3 has the request body as an separate object instead of an "body" parameter, to avoid conflict,
    // there should be no requestBody additionally in parameters
    if (!apiOperationData.parameters?.requestBody && apiOperationData?.requestBody?.content) {
      const requestBodyData = apiOperationData.requestBody.content[node.requestContentType]
      if (requestBodyData) {
        node.parameters.requestBody = {
          name: 'Request body',
          required: !!apiOperationData.requestBody.required || false,
          isActive: !!apiOperationData.requestBody.required || oldParameters.requestBody?.isActive || false,
          activeFilter: oldParameters.requestBody?.activeFilter || '',
          type: oldParameters.requestBody?.type || getTypedInputType(requestBodyData),
          parameters: {}, // using for UI editor
          mark: apiOperationData.requestBody.mark || { name: false, type: false, value: false, activeFilter: false },
          msgPathString: 'payload' // more correct would be payload.requestBody as another parameter could have the same name, but this is not practical.
        }

        // value is for not using UI editor, but should be set, if changing type
        node.parameters.requestBody.value = (typeof oldParameters.requestBody?.value !== 'undefined')
          ? oldParameters.requestBody.value
          : (node.parameters.requestBody.type === 'json')
              ? '{}'
              : ''

        // save description to requestBodyData (schema)
        requestBodyData.schema.description = requestBodyData.schema.description || apiOperationData.requestBody.description || ''
        // create subparameters. check by schema to remove legacy parameters
        let schema = {}
        const multiSchemesType = getMultipleSchemesType(requestBodyData.schema)
        if (multiSchemesType) {
          const selectedSchema = oldParameters.requestBody?.selectedSchema || {}
          const selectedType = selectedSchema.type || multiSchemesType
          let selectedSchemaIndex = requestBodyData.schema[multiSchemesType].findIndex(s => s?.$$ref?.endsWith(selectedSchema.name))
          selectedSchemaIndex = selectedSchemaIndex >= 0 ? selectedSchemaIndex : 0
          schema = requestBodyData.schema[selectedType]?.[selectedSchemaIndex] || {}
          selectedSchema.type = selectedType
          selectedSchema.name = requestBodyData.schema[multiSchemesType][selectedSchemaIndex].$$ref.split('/').pop()
          node.parameters.requestBody.selectedSchema = selectedSchema
        } else if (requestBodyData.schema.properties) {
          schema = requestBodyData.schema
        }

        Object.keys(schema.properties || {}).forEach(property => {
          const oldParameterValues = oldParameters.requestBody?.parameters?.[property]
          const paramData = schema.properties[property]
          const requiredByParent = schema.required?.includes(property)
          node.parameters.requestBody.parameters[property] = createParameter(property, oldParameterValues, paramData, requiredByParent, 'payload')
        })
      }
    }
    // add default parameters
    const apiParameterSchemes = apiOperationData.parameters?.sort(orderRequired) || []
    apiParameterSchemes.forEach(parameterMetaData => {
      // Not good style, but a parameter with the same name can be in path, query, header or cookie
      // There was a ticket a long time ago...
      const paramKey = parameterMetaData.name + ' ' + parameterMetaData.in
      const oldParam = oldParameters[paramKey] || {}
      node.parameters[paramKey] = createParameter(parameterMetaData.name, oldParam, parameterMetaData)
    })
  } catch (e) {
    console.error(e)
    setError(node, 'Error creating parameters. Please check your openApi specification.', 'createParameters')
  }
}

module.exports = {
  clearError,
  createOutputs,
  getAllowedTypes,
  getTypedInputType,
  getMultipleSchemesType,
  getMultiSelectionSelectedSchema,
  createParameter,
  createParameters,
  setError
}
