const Swagger = require('swagger-client')
const util = require('util')
let nodeFetch // lazy import below

module.exports = function (RED) {
  // syncronous evaluateNodeProperty is depreceated with Node-RED 3.1
  // https://github.com/flowforge/flowforge-nr-dashboard/issues/99
  const evalNodeProp = util.promisify(RED.util.evaluateNodeProperty)

  function openApiRed (config) {
    RED.nodes.createNode(this, config)
    const node = this

    // openApi-red >= v.2.0.0 uses parameter object instead of array
    const restructureParamFromArray = (parameters) => {
      console.warn('[openAPI-red] Warning: Depreceated parameter format. Please use an object instead of an array.')
      const newParameterObject = {}
      parameters.forEach(parameter => {
        newParameterObject[parameter.name] = parameter
      })
      return newParameterObject
    }

    // return -1 if not found
    const findOutputNumber = (response) => {
      const responseCode = response.code || response.statusCode || response.status || ''
      let outputNumber
      if (config.outputStyle === 'compact') {
        outputNumber = config.responseOutputLabels.findIndex(label => label.code === (responseCode?.toString()?.substring(0, 1) + 'xx'))
      } else if (config.outputStyle === 'each response') {
        outputNumber = config.responseOutputLabels.findIndex(label => Number(label.code) === responseCode)
      }
      if (outputNumber === -1 && response.ok) {
        // check for unspecified successful output. this should always be index === 0
        // This is only available if no successful output was defined. Else it will go to undefined responses.
        outputNumber = config.responseOutputLabels.findIndex(label => label.code === 'successful' || label.code === 'all')
      }

      // still no defined output found, exist an undefined response output?
      if (outputNumber === -1 && config.errorHandling === 'other output') {
        outputNumber = config.responseOutputLabels.findIndex(label => label.code === 'undefinedResponses')
      }
      return outputNumber
    }

    const setMessage = async (msg, response) => {
      if (typeof response === 'string') {
        msg.payload = response
      } else if (config.responseAsPayload) {
        // legacy mode
        msg.payload = response
      } else {
        // More about the duplicates: https://github.com/swagger-api/swagger-js/blob/96d261987700297a472db5587c0a8c769095d73e/src/http/index.js#L115
        delete response.obj // Duplicate of response.body
        // If response is a file, convert blob as NR (or express) will remove it on sending
        if (response?.data instanceof Blob) {
          msg.payload = Buffer.from(await response.data.arrayBuffer())
          const contentDisposition = response?.headers?.['content-disposition'] || ''
          // check if filename is stated
          // do not split via ";" as this is a legit char for filenames ('"' is not valid for windows, but prob. ok for linux)
          let filenameStartIndex = contentDisposition.indexOf('filename="')
          if (filenameStartIndex > -1) {
            filenameStartIndex += 10 // + filename="
            const filename = contentDisposition.substring(filenameStartIndex)?.split('"')?.[0]
            if (filename) {
              msg.filename = filename
            }
          }
        } else {
          msg.payload = response.body
        }
        delete response.data // Duplicate of response.text
        msg.response = response
        delete msg.response.body
      }
    }

    node.on('input', async function (msg, send, done) {
      const handleDebugMode = (request) => {
        // if we have the full request object from swagger-client
        msg.openApiDebugData = { ...(request || requestSettings), ...debugData }
        delete msg.openApiDebugData.requestInterceptor
        if (msg.openApiDebugData.operationId) {
          delete msg.openApiDebugData.pathName
          delete msg.openApiDebugData.method
        }
        node.warn(msg.openApiDebugData)
      }

      const checkParameterIsActive = (param) => param.isActive || param.required || false

      const checkParamActiveFilter = async (param, context = {}, inArray = false) => {
        // if required parameter is iterated, each "yield" must be checked
        if (param.required && !inArray) {
          return true
        }
        let isActive = param.isActive
        if (isActive && param.activeFilter) {
          const filterFunction =  new Function('msg', '{' + Object.keys(context).join (',') + '}', param.activeFilter) // eslint-disable-line
          isActive = filterFunction(msg, context)
        }
        return isActive
      }

      const evaluateNodeProp = async (value, type, thiz, msg, context) => {
        try {
          // using try/catch to return a more user friendly error
          let result
          if (type.startsWith('_each_')) {
            const source = context[type.substring(6)]
            if (value.startsWith('.')) {
              value = value.substring(1)
            }
            // get nested value from object or array
            result = value.split(/[\.\[\]\'\"]/).filter(p => p).reduce((o, p) => o ? o[p] : undefined, source) // eslint-disable-line
          } else {
            result = await evalNodeProp(value, type, thiz, msg)
          }
          return result
        } catch (e) {
          console.error(e)
          throw new Error(`[openAPI-red] Could not evaluate value "${value}" from type "${type}".`)
        }
      }

      const sendError = async (e, requestSettings) => {
        node.status({ fill: 'red', shape: 'dot', text: 'Error code: ' + (e.code || e.statusCode || e.status || 'Unknown code') })
        const errorMsg = `${e.status || e.response?.body?.code || ''} ${e.message} ${e.response?.body?.message ? '- ' + e.response.body.message : ''}`
        await setMessage(msg, e.response || e.message)

        if (config.debugMode && requestSettings) {
          handleDebugMode()
        }

        if (config.outputStyle === 'classic') {
          if (config.errorHandling === 'other output') {
            send([null, msg])
            done()
          } else if (config.errorHandling === 'throw exception') {
            done(errorMsg)
          } else {
            send(msg)
            done()
          }
        } else if (config.outputStyle === 'each response' || config.outputStyle === 'compact') {
          // check for an existing response output (includes check for error handling "other output")
          const outputNumber = findOutputNumber(e)
          if (outputNumber > -1) {
            const msgArray = Array(outputNumber).fill(null)
            msgArray.push(msg)
            send(msgArray)
            done()
          } else {
            // if no output found, throw expection
            done(e)
          }
        }
      }

      // context will be recreated for each main parameter
      const createSpecialValue = async (param, alreadyNested = false, context = {}) => {
        let value
        context.value = null // next value
        // check only if parameter is active or required for now, the filter check must be done individually (e.g. parameter is required, but the yield for an array/each shall be filtered)
        if (checkParameterIsActive(param)) {
          if (param.type.startsWith('editor')) {
            // check if the whole editor should be added
            if (await checkParamActiveFilter(param, context)) {
              value = alreadyNested ? {} : { [param.name]: {} }
              for await (const subParam of Object.values(param.parameters || {})) {
                context.value = await createSpecialValue(subParam, true, context)
                if (await checkParamActiveFilter(subParam, context)) {
                  // await checkIsParameterActive(subParam, eachIndex)
                  if (alreadyNested) {
                    value[subParam.name] = context.value
                  } else {
                    value[param.name][subParam.name] = context.value
                  }
                }
              }
            }
          } else if (param.type === 'array') {
            value = []
            for await (const arrayParam of param.value) {
              if (await checkParamActiveFilter(arrayParam, context, true)) {
                const evaluatedValue = await createSpecialValue(arrayParam, true, context)
                value.push(evaluatedValue)
              }
            }
          } else if (param.type === 'select') {
            try {
              context.value = JSON.parse(param.value)
            } catch (e) {
              // undefined (if somebody really want to make that selectable....) or other error, send at least the string value
              context.value = param.value
            }
            if (await checkParamActiveFilter(param, context)) {
              value = context.value
            }
          } else if (param.type === 'each') {
            // check if each  is active
            if (await checkParamActiveFilter(param, context)) {
              const eachValues = await evaluateNodeProp(param.each, param.eachType, this, msg, context)
              value = []
              if (!Array.isArray(eachValues)) {
                throw new Error('[openAPI-red] Parameter "' + param.name + '" is from type "each", but it\'s value is not an array.')
              }
              const parameterDefinition = param.value[0]
              for (let index = 0; index < eachValues.length; index++) {
                // each values must be checked individually (yield)
                context[param.as] = eachValues[index]
                if (await checkParamActiveFilter(parameterDefinition, context, true)) {
                  const evaluatedValue = await createSpecialValue(parameterDefinition, true, context)
                  value.push(evaluatedValue)
                }
              }
              // all children of each have been evaluated -> remove context[param.as] to hide it from later context accesses
              delete context[param.as]
            }
          } else {
            context.value = await evaluateNodeProp(param.value, param.type, this, msg, context)
            if (await checkParamActiveFilter(param, context)) {
              value = context.value
            }
          }
        }
        return value
      }

      const configNode = RED.nodes.getNode(config.configUrlNode) || {}
      let parameters = {}
      let debugData = {}
      // let eachValues = {}
      // eslint-disable-next-line
      let requestBody = undefined

      send = send || function () { node.send.apply(node, arguments) }

      if (msg.openApi?.parameters) {
        parameters = msg.openApi.parameters
        if (Array.isArray(parameters)) {
          parameters = restructureParamFromArray(parameters)
        }
      } else {
        if (Array.isArray(config.parameters)) {
          config.parameters = restructureParamFromArray(config.parameters)
        }
        try {
          const parameterKeys = Object.keys(config.parameters)
          // forEach cannot be async...
          for await (const pKey of parameterKeys) {
            const parameter = config.parameters[pKey]
            // check first lvl parameters (requestBody, query parameter,...)
            if (checkParameterIsActive(parameter)) {
              let parameterValue
              const context = {}
              if (parameter.type.startsWith('editor')) {
                // recursive build of object -> e.g. returns { body: { id: 123 } }
                // but it must be set to parameters.body (below) -> only the value of the object is needed
                // (no need to put value into context)
                parameterValue = await createSpecialValue(parameter, false, context)
                parameterValue = parameterValue[parameter.name] // remove first object level ({Request body: {} })
              } else if (parameter.type === 'array') {
                if (!Array.isArray(parameter.value)) {
                  throw new Error('[openAPI-red] Parameter "' + parameter.name + '" is from type array, but it\'s value not.')
                }
                if (await checkParamActiveFilter(parameter, context)) {
                  parameterValue = await createSpecialValue(parameter, false, context)
                }
              } else if (parameter.type === 'each') {
                if (await checkParamActiveFilter(parameter, context)) {
                  parameterValue = await createSpecialValue(parameter, false, context)
                }
              } else {
                context.value = await evaluateNodeProp(parameter.value, parameter.type, this, msg)
                if (await checkParamActiveFilter(parameter, context)) {
                  parameterValue = context.value
                }
              }
              // query input can't be object, swagger.js should handle this but does not (https://github.com/swagger-api/swagger-js/blob/master/docs/usage/http-client.md#query-support)
              // arrays still work and will be set to "...tag=val1&tag=val2"
              if (typeof parameterValue === 'object' && !Array.isArray(parameterValue) && parameter.in === 'query') {
                parameterValue = JSON.stringify(parameterValue)
              }
              if (parameter.name === 'Request body') {
                requestBody = parameterValue
              } else {
                parameters[parameter.name] = parameterValue
              }
            }
          }
        } catch (e) {
          console.log('[openAPI-red] Error creating parameters')
          sendError(e)
          return
        }
      }

      // fallback if no content type can be found
      let requestContentType = 'application/json'
      if (config.requestContentType) requestContentType = config.requestContentType
      const opData = config.operationData
      // try to get it if source was unavailable on startup (e.g. NodeRed creates specification or server/shuttle was not ready yet)
      let spec
      try {
        spec = configNode?.openApiSpecification() || null
        if (!spec) {
          await configNode.loadOpenApiSpec({
            id: configNode.id,
            apiSource: configNode.url,
            sourceType: configNode.urlType,
            serverUrl: configNode.server,
            serverType: configNode.serverType,
            devMode: configNode.devMode
          })
          spec = configNode.openApiSpecification()
        }

        // upgrade to v.2 needs operation method (node was not opened yet after upgrade)
        if (!opData.method && opData.hasOperationId && opData.id) {
          opData.method = Object.keys(spec.paths[opData.path] || {}).find(method => spec.paths[opData.path][method]?.operationId === opData.id)
        }
        // resolve if neccessary (this is neccessary once, if the node editor was not opened)
        if (!spec.paths[opData.path][opData.method]['x-openApi-red-resolved']) {
          await Swagger.resolveSubtree(spec, ['paths', opData.path, opData.method])
          spec.paths[opData.path][opData.method]['x-openApi-red-resolved'] = true
        }
      } catch (e) {
        // ignore error -> do not let NR crash
        console.log(e)
      }
      if (!spec) {
        sendError(new Error('No openApi specification found. Please check the config node.'))
        return
      }

      const requestSettings = {
        // preferred use is operationId. If not available use pathname + method
        operationId: opData.hasOperationId ? opData.id : undefined,
        pathName: opData.hasOperationId ? undefined : opData.path,
        method: opData.hasOperationId ? undefined : opData.method,
        parameters,
        requestBody,
        requestContentType,
        requestInterceptor: async (req) => {
          // add headers from config node / node / msg
          req.headers = req.headers || {}
          if (configNode.headers?.length) {
            for (let index = 0; index < configNode.headers.length; index++) {
              req.headers[configNode.headers[index].key] = await evaluateNodeProp(configNode.headers[index].value, configNode.headers[index].valueType, this, msg)
            }
          }
          if (config.headers?.length) {
            for (let index = 0; index < config.headers.length; index++) {
              req.headers[config.headers[index].key] = await evaluateNodeProp(config.headers[index].value, config.headers[index].valueType, this, msg)
            }
          }
          if (msg.headers) {
            req.headers = Object.assign(req.headers, msg.headers)
          }
          // depreceated: remove in next major version
          if (msg.openApiToken) {
            req.headers.Authorization = 'Bearer ' + msg.openApiToken
            this.warn('msg.openApiToken is depreceated. Please use msg.headers')
          }
          // remove keys with empty value
          Object.keys(req.headers).forEach(key => {
            if (!req.headers[key]) {
              delete req.headers[key]
            }
          })

          if (config.debugMode) {
            debugData = req
          }
          if (!config.keepAuth) {
            delete msg.openApiToken
            delete msg.headers
          }
        }
      }
      if (config.responseContentType) {
        requestSettings.responseContentType = config.responseContentType
      }

      if (spec.swagger?.startsWith('2')) {
        if (configNode.server) {
          requestSettings.contextUrl = configNode.server
        }
      } else {
        // will only work with openApi v3
        // important: the custom server must be set into the specs, else it will be ignored
        if (['msg', 'flow', 'global'].includes(configNode.serverType)) {
          requestSettings.server = await evaluateNodeProp(configNode.server, configNode.serverType, this, msg)
          if (!spec.servers.find(server => server.url === requestSettings.server)) {
            spec.servers.push({ url: requestSettings.server })
          }
        } else {
          requestSettings.server = configNode.server || spec.openApiRed?.defaultServer // this option is already set into the specs
        }
      }

      // localhost can accept self signed signatures
      if (requestSettings.server?.startsWith('https://localhost:') || requestSettings.server?.startsWith('https://localhost/') || node.devMode) {
        if (!nodeFetch) {
          nodeFetch = (await import('node-fetch')).default
        }
        const agent = require('https').Agent({
          rejectUnauthorized: false
        })
        requestSettings.userFetch = (url, options) => {
          options.agent = url.startsWith('https') ? agent : undefined
          return nodeFetch(url, options)
        }
      }

      node.status({ fill: 'yellow', shape: 'dot', text: 'Retrieving...' })
      try {
        const response = await Swagger.execute({ spec, ...requestSettings })
        node.status({})
        if (config.debugMode) {
          handleDebugMode()
        }
        await setMessage(msg, response)
        if (config.outputStyle === 'classic') {
          send(msg)
          done()
        } else {
          const outputNumber = findOutputNumber(response)
          if (outputNumber > -1) {
            const msgArray = Array(outputNumber).fill(null)
            msgArray.push(msg)
            send(msgArray)
          } else {
            // No output found -> throw exception
            sendError(response, requestSettings)
          }
        }
      } catch (e) {
        // invalid input or server returns error
        sendError(e, requestSettings)
      }
    })
  }
  RED.nodes.registerType('openApi-red', openApiRed)
}
